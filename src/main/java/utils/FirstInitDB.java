package utils;

import dao.DishDAOImpl;
import models.Dish;

public class FirstInitDB {
	private static String[] names = {"Плов","Борщ","Беляш","Компот","Чай","Кофе"};
	private static String[] descriptions = {"Из настоящей баранины. Морковь и рис без ГМО.","По рецепту бабушки, взятому из ближайшего справочника","Фарш настолько качественно обработан, что мясо на вкус покажется говядиной","Помните Yupi","Чай","Кофе"};
	private static double[] prices = {37.5, 77.8, 80.5, 10.0, 15.0, 20.0};
	private static double[] weight = {0.4, 0.5, 0.25, 0.25, 0.25, 0.25};
	public static void initDB() {
    	DishDAOImpl daoD = new DishDAOImpl();
    	if( daoD.findAll().size() == 0 ) {
    		for(int i = 0; i < names.length; i++) {
    			Dish dish = new Dish();
    			dish.setName(names[i]);
    			dish.setDescription(descriptions[i]);
    			dish.setPrice(prices[i]);
    			dish.setWeight(weight[i]);
    			daoD = new DishDAOImpl();
    			daoD.save(dish);
    		}
    	}
	}
}
