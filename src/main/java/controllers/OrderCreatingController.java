package controllers;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import dao.DishDAOImpl;
import dao.OrderDAOImpl;
import dao.OrderedDishesDAOImpl;
import dao.UserDAOImpl;
import models.Dish;
import models.Orders;
import models.OrderedDishes;
import models.User;

public class OrderCreatingController {
	private User user;
	private Orders orders;
	private HashMap<Long, Integer> mapDishes;
	public OrderCreatingController() {}
	public OrderCreatingController(User user, HashMap<Long, Integer> mapDishes) {
		setUser(user);
		setMapDishes(mapDishes);
	}
	
	public void createOrder() {
		UserDAOImpl daoUser = new UserDAOImpl();
		daoUser.save(user);
		orders = new Orders();
		orders.setDateDelivery(LocalDateTime.now());
		orders.setUser(user);
		OrderDAOImpl daoOrder = new OrderDAOImpl();
		daoOrder.save(orders);
		for(Entry<Long, Integer> entry : mapDishes.entrySet()) {
			DishDAOImpl daoDish = new DishDAOImpl();
			Dish dish = daoDish.findById(entry.getKey());
			OrderedDishes ordDishes = new OrderedDishes();
			ordDishes.setDish(dish);
			ordDishes.setOrder(orders);
			ordDishes.setNumberOfServings(entry.getValue());
			OrderedDishesDAOImpl ordDish = new OrderedDishesDAOImpl();
			ordDish.save(ordDishes);
		}
	}
	
	public String getMessageOrder() {
		String msg = "";
		OrderDAOImpl daoOrder = new OrderDAOImpl();
		orders = daoOrder.findLast();
		UserDAOImpl daoImpl = new UserDAOImpl();
		User userItem =  daoImpl.findById(orders.getUser().getId());
		msg = "№ Заказа: " + orders.getId() + "\nИмя заказчика: " + userItem.getName() + 
				"\nАдрес: " + userItem.getAddress() + "\nТелефон: " + userItem.getTelephone()+ "\nСвединия о заказе:\n";
		OrderedDishesDAOImpl ordDao = new OrderedDishesDAOImpl();
		ArrayList<OrderedDishes> ordDishes = (ArrayList<OrderedDishes>) ordDao.findAll();
		double price = 0.0, weight = 0.0;
		for(OrderedDishes item: ordDishes) {
			if(item.getOrder().getId() == orders.getId()) {
				DishDAOImpl dishDao = new DishDAOImpl();
				Dish dish = dishDao.findById(item.getDish().getId());
				msg += dish.getName() + " | " + dish.getDescription() + " | " + dish.getPrice() + " руб |" + item.getNumberOfServings() + " порц.\n";
				price += dish.getPrice()* item.getNumberOfServings();
				weight += dish.getWeight()*item.getNumberOfServings();
			}
		}
		msg += "\n   Вес: " + weight + "\nЦена: " + price;
		return msg;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public HashMap<Long, Integer> getMapDishes() {
		return mapDishes;
	}
	public void setMapDishes(HashMap<Long, Integer> mapDishes) {
		this.mapDishes = mapDishes;
	}
}
