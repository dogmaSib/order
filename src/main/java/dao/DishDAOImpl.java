package dao;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import models.Dish;
import utils.HibernateUtil;

public class DishDAOImpl implements ObjectDAO<Integer, Dish> {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Dish> findAll() {
		Session session = null;
		List<Dish> dishes = new ArrayList<Dish>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			dishes = session.createCriteria(Dish.class).list();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'getAll'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return dishes;
	}

	public List<Dish> findAllWithDetail() {
		// TODO Auto-generated method stub
		return null;
	}

	public Dish findById(Long id) {
		Session session = null;
		Dish dish = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	    	dish = (Dish) session.load(Dish.class, id);
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		return dish;
	}

	public Dish save(Dish dish) {
		Session session = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	      	session.beginTransaction();
	      	session.save(dish);
	      	session.getTransaction().commit();
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
	    } finally {
	    	if (session != null && session.isOpen()) {
	    		session.close();
	    	}
	    }
		return null;
	}
	public void delete(Dish dish) {
		Session session = null;
		try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(dish);
		  session.getTransaction().commit();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при удалении", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}	
	}
}
