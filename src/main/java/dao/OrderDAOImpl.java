package dao;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import models.Orders;
import utils.HibernateUtil;

public class OrderDAOImpl implements ObjectDAO<Integer, Orders> {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Orders> findAll() {
		Session session = null;
		List<Orders> orders = new ArrayList<Orders>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			orders = session.createCriteria(Orders.class).list();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'getAll'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return orders;
	}

	public List<Orders> findAllWithDetail() {
		// TODO Auto-generated method stub
		return null;
	}

	public Orders findById(Long id) {
		Session session = null;
		Orders orders = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	    	orders = (Orders) session.load(Orders.class, id);
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		return orders;
	}

	public Orders save(Orders orders) {
		Session session = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	      	session.beginTransaction();
	      	session.save(orders);
	      	session.getTransaction().commit();
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
	    } finally {
	    	if (session != null && session.isOpen()) {
	    		session.close();
	    	}
	    }
		return null;
	}

	public void delete(Orders orders) {
		Session session = null;
		try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(orders);
		  session.getTransaction().commit();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при удалении", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}	
	}

	public Orders findLast() {
		Session session = null;
		Orders orders = null;
		try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	    	orders  =  (Orders) session.createQuery("FROM Orders o ORDER BY o.id DESC").setMaxResults(1).list().get(0);
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		return orders;
	}
}
