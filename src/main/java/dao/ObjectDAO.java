package dao;

import java.util.List;

public interface ObjectDAO<K,V> {
    public List<V> findAll();
 
    public List<V> findAllWithDetail();
 
    public V findById(Long id);
 
    // Вставить или обновить контакт.
    public V save(V dish);
 
    // Удалить .
    public void delete(V dish);

}
