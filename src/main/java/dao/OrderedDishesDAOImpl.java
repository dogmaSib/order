package dao;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import models.OrderedDishes;
import utils.HibernateUtil;

public class OrderedDishesDAOImpl implements ObjectDAO<Integer, OrderedDishes> {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<OrderedDishes> findAll() {
		Session session = null;
		List<OrderedDishes> orderedDishes = new ArrayList<OrderedDishes>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			orderedDishes = session.createCriteria(OrderedDishes.class).list();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'getAll'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return orderedDishes;
	}

	public List<OrderedDishes> findAllWithDetail() {
		// TODO Auto-generated method stub
		return null;
	}

	public OrderedDishes findById(Long id) {
		Session session = null;
		OrderedDishes orderedDishes = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	    	orderedDishes = (OrderedDishes) session.load(OrderedDishes.class, id);
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		return orderedDishes;
	}

	public OrderedDishes save(OrderedDishes order) {
		Session session = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	      	session.beginTransaction();
	      	session.save(order);
	      	session.getTransaction().commit();
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
	    } finally {
	    	if (session != null && session.isOpen()) {
	    		session.close();
	    	}
	    }
		return null;
	}

	public void delete(OrderedDishes orderedDishes) {
		Session session = null;
		try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(orderedDishes);
		  session.getTransaction().commit();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при удалении", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}	
	}
}
