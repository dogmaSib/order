package dao;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import models.User;
import utils.HibernateUtil;

public class UserDAOImpl implements ObjectDAO<Integer, User>{

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<User> findAll() {
		Session session = null;
		List<User> users = new ArrayList<User>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			users = session.createCriteria(User.class).list();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'getAll'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return users;
	}

	public List<User> findAllWithDetail() {
		// TODO Auto-generated method stub
		return null;
	}

	public User findById(Long id) {
		Session session = null;
		User user = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	    	user = (User) session.load(User.class, id);
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		return user;
	}

	public User save(User user) {
		Session session = null;
	    try {
	    	session = HibernateUtil.getSessionFactory().openSession();
	      	session.beginTransaction();
	      	session.save(user);
	      	session.getTransaction().commit();
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
	    } finally {
	    	if (session != null && session.isOpen()) {
	    		session.close();
	    	}
	    }
		return null;
	}

	public void delete(User user) {
		Session session = null;
		try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(user);
		  session.getTransaction().commit();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при удалении", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
				HibernateUtil.getSessionFactory().close();
		    }
		}	
	}
}
