package order.food.FoodOrder;

import java.sql.SQLException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
/*
import dao.DishDAOImpl;
import dao.OrderDAOImpl;
import dao.OrderedDishesDAOImpl;
import dao.UserDAOImpl;
import models.Dish;
import models.Orders;
import models.OrderedDishes;
import models.User;
*/
import utils.FirstInitDB;
import views.OrderView;

/**
 * Hello world!
 *
 */
public class App{
    public static void main( String[] args ) throws SQLException{
    	FirstInitDB.initDB();
/* just for test
     	UserDAOImpl daoU = new UserDAOImpl();
    	User user = new User();
    	user.setName("igor");
    	user.setAddress("NSK");
    	user.setTelephone("9879898987");
    	daoU.save(user);
    	
    	OrderDAOImpl daoO = new OrderDAOImpl();
    	Orders order = new Orders();
        LocalDateTime rightNow = LocalDateTime.now();
    	order.setDateDelivery(rightNow);
    	order.setPrice(200.5);
    	order.setUser(user);
    	daoO.save(order);
    	
    	OrderedDishesDAOImpl daoOD = new OrderedDishesDAOImpl();
    	OrderedDishes di = new OrderedDishes();
    	di.setDish(dish);
    	di.setOrder(order);
    	di.setNumberOfServings(2);
    	daoOD.save(di);
    	
    	OrderedDishes di1 = new OrderedDishes();
    	di1.setDish(dish1);
    	di1.setOrder(order);
    	di1.setNumberOfServings(4);
    	daoOD.save(di1);
    	*/
  	   	Display display = new Display();
    	Shell shell = new OrderView().createShell(display);
    	shell.open();
    	while (!shell.isDisposed()) {
    		if (!display.readAndDispatch())
    			display.sleep();
    	}

    }
}
