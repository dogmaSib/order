package models;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "OrderedDishes")
public class OrderedDishes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6376042568465009050L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)	
	private Long id;
	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	private Orders orders;
	@ManyToOne
	@JoinColumn(name = "dish_id", nullable = false)
	private Dish dish;
	@Column(name = "number_of_servings", unique = false, nullable = false)
	private int numberOfServings;
	public OrderedDishes() {}
	public OrderedDishes(Long idOrder, int idProduct, int numberOfServings) {
		setId(idOrder);
	//	setIdProduct(idProduct);
		setNumberOfServings(numberOfServings);
	}
	@SuppressWarnings("unused")
	private Long getId() {
		return id;
	}
	private void setId(Long idOrder) {
		this.id = idOrder;
	}
	public Orders getOrder() {
		return orders;
	}
	public void setOrder(Orders orders) {
		this.orders = orders;
	}
	public Dish getDish() {
		return dish;
	}
	public void setDish(Dish dish) {
		this.dish = dish;
	}
	public int getNumberOfServings() {
		return numberOfServings;
	}
	public void setNumberOfServings(int numberOfServings) {
		this.numberOfServings = numberOfServings;
	}
}
