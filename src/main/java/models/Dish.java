package models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Dish")
public class Dish implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5828445932401718675L;
	private Long id;
	private String name;
	private String description;
	private double price;
	@Column(name = "WEIGHT", unique = false, nullable = false)
	private double weight;
	private Set<OrderedDishes> orderedDishes = new HashSet<OrderedDishes>();
	public Dish() {}
	public Dish(Long id, String name, String description, double price, double weight) {
		setId(id);
		setName(name);
		setDescription(description);
		setPrice(price);
		setWeight(weight);
	}
	
	@OneToMany(mappedBy = "dish", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<OrderedDishes> getOrderedDishes() {
		return orderedDishes;
	}
	public void setOrderedDishes(Set<OrderedDishes> orderedDishes) {
		this.orderedDishes = orderedDishes;
	}
	@Column(name = "NAME", unique = false, nullable = false, length = 100)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "DESCRIPTION", unique = false, nullable = false, length = 200)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name = "PRICE", unique = false, nullable = false)
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	@Id
	@Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
