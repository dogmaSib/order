package models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Orders", uniqueConstraints = {@UniqueConstraint(columnNames = "id")})
public class Orders implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5110084378178564806L;
	@Id
	@Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "date_delivery", unique = false, nullable = false, length = 100)
	private LocalDateTime dateDelivery;
	@Column(name = "price", unique = false, nullable = false, length = 100)
	private double price;
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	@OneToMany(mappedBy = "orders", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private Set<OrderedDishes> orderedDishes;
	// empty constructor for Hibernate
	public Orders() {}
	// object constructor
	public Orders(Long id, int idUser, LocalDateTime dateDelivery, double price) {
		setId(id);
		setDateDelivery(dateDelivery);
		setPrice(price);
	}
	public Set<OrderedDishes> getOrderedDishes() {
		return orderedDishes;
	}
	public void setOrderedDishes(Set<OrderedDishes> orderedDishes) {
		this.orderedDishes = orderedDishes;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDateTime getDateDelivery() {
		return dateDelivery;
	}
	public void setDateDelivery(LocalDateTime dateDelivery) {
		this.dateDelivery = dateDelivery;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
