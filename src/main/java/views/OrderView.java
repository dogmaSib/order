package views;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import controllers.OrderCreatingController;
import dao.DishDAOImpl;
import models.Dish;
import models.User;

public class OrderView {
	Text userName;
	Text userAddress;
	DateTime panelTime;
	DateTime timeDelivery;
	Text userPhone;
	Table categories;
	
	public Shell createShell(final Display display) throws SQLException {
		Shell shell = new Shell(display);
	    shell.setText("Оформление заказа");
	    drawUserDetails(shell);	    
	    drawOrderDetails(shell);
	    
	    shell.pack();
	    return shell;
    }
	
	private void drawUserDetails(Shell shell) {
		GridLayout gridLayout = new GridLayout();
	    gridLayout.numColumns = 3;
	    shell.setLayout(gridLayout);
	    GridData gridData ;
	    
	    Group userInfo = new Group(shell, SWT.NONE);
	    userInfo.setText("Информация о заказчике");
	    gridLayout = new GridLayout();
	    gridLayout.numColumns = 2;
	    userInfo.setLayout(gridLayout);
	    gridData = new GridData(GridData.FILL, GridData.CENTER, true, false);
	    gridData.horizontalSpan = 2;
	    userInfo.setLayoutData(gridData);

	    new Label(userInfo, SWT.NONE).setText("Ф. И. О. :");
	    userName = new Text(userInfo, SWT.SINGLE | SWT.BORDER);
	    gridData = new GridData(GridData.FILL, GridData.CENTER, true, false);
	   // gridData.horizontalSpan = 3;
	    userName.setLayoutData(gridData);
	    
	    new Label(userInfo, SWT.NONE).setText("Адрес доставки:");
	    userAddress = new Text(userInfo, SWT.SINGLE | SWT.BORDER);
	    userAddress.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
	    
	    new Label(userInfo, SWT.NONE).setText("Телефон:");
	    userPhone = new Text(userInfo, SWT.SINGLE | SWT.BORDER);
	    userPhone.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

	}
	private void drawOrderDetails(final Shell shell) {
	    Group orderInfo = new Group(shell, SWT.NONE);
	    orderInfo.setText("Информация о заказе");
	    GridLayout gridLayout = new GridLayout();
	    gridLayout.numColumns = 1;
	    orderInfo.setLayout(gridLayout);
	    GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false);
	    gridData.horizontalSpan = 4;
	    orderInfo.setLayoutData(gridData);

	    new Label(orderInfo, SWT.NONE).setText("Дата доставки:");
	    panelTime = new DateTime(orderInfo, SWT.BORDER|SWT.DATE);
	    new Label(orderInfo, SWT.NONE).setText("Время доставки:");
	    timeDelivery = new DateTime(orderInfo, SWT.BORDER|SWT.TIME);
	    
	    Label label = new Label(orderInfo, SWT.NONE);
	    label.setText("Формирование заказа");
	    label.setLayoutData(new GridData(GridData.FILL_BOTH, GridData.CENTER, true, false));
	    
	    categories = new Table(orderInfo, SWT.CHECK |SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION );
	    
	    TableColumn isEnable = new TableColumn(categories, SWT.CENTER | SWT.BORDER);
	    isEnable.setText("Выбрать");
	    isEnable.setWidth(60);
	   
	    TableColumn productName = new TableColumn(categories, SWT.CENTER | SWT.BORDER);
	    productName.setText("Название");
	    productName.setWidth(150);
	    
	    TableColumn productDescription = new TableColumn(categories, SWT.CENTER | SWT.BORDER);
	    productDescription.setText("Описание");
	    productDescription.setWidth(400);
	    
	    TableColumn productPrice = new TableColumn(categories, SWT.CENTER | SWT.BORDER);
	    productPrice.setText("Цена");
	    productPrice.setWidth(60);
	    
	    TableColumn productWeight = new TableColumn(categories, SWT.CENTER | SWT.BORDER);
	    productWeight.setText("Вес(кг)");
	    productWeight.setWidth(60);
	    
	    TableColumn productIsEnable = new TableColumn(categories, SWT.CENTER);
	    productIsEnable.setText("Количество");
	    productIsEnable.setWidth(60);
	    
	    categories.setHeaderVisible(true);
	    DishDAOImpl dao = new DishDAOImpl();
		List<Dish> dishes = dao.findAll(); 
	    for(int i = 0; i < dishes.size(); i ++) {
	        new TableItem(categories, SWT.NONE);
		}
		 
	    final TableItem[] items = categories.getItems();
	    for(int i = 0; i < dishes.size(); i ++) {
	    	Dish dish = dishes.get(i);
	        items[i].setText(0, " " + dish.getId());
	        items[i].setText(1, dish.getName());
	        items[i].setText(2, dish.getDescription());
	        items[i].setText(3, "" + dish.getPrice());
	        items[i].setText(4, "" + dish.getWeight());
	        items[i].setText(5, "1");
	    }
	    final TableEditor editor = new TableEditor(categories);
        // The editor must have the same size as the cell and must
        // not be any smaller than 50 pixels.
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = 50;
        // editing the second column
        final int EDITABLECOLUMN = 5;

        categories.addSelectionListener(new SelectionAdapter() {
          public void widgetSelected(SelectionEvent e) {
            // Clean up any previous editor control
            Control oldEditor = editor.getEditor();
            if (oldEditor != null)
              oldEditor.dispose();

            // Identify the selected row
            TableItem item = (TableItem) e.item;
            if (item == null)
              return;

            // The control that will be the editor must be a child of the Table
            Text newEditor = new Text(categories, SWT.NONE);
            newEditor.setText(item.getText(EDITABLECOLUMN));
            newEditor.addModifyListener(new ModifyListener() {
              public void modifyText(ModifyEvent me) {
                Text text = (Text) editor.getEditor();
                editor.getItem().setText(EDITABLECOLUMN, text.getText());
              }
            });
            newEditor.selectAll();
            newEditor.setFocus();
            editor.setEditor(newEditor, item, EDITABLECOLUMN);
          }
        });

	    gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
	    gridData.verticalSpan = 4;
	    int listHeight = categories.getItemHeight() * 12;
	    Rectangle trim = categories.computeTrim(0, 0, 0, listHeight);
	    gridData.heightHint = trim.height;
	    categories.setLayoutData(gridData);
	    
	    Button enter = new Button(shell, SWT.PUSH);
	    enter.setText("Enter");
	    gridData = new GridData(GridData.END, GridData.CENTER, false, false);
	    gridData.horizontalSpan = 2;
	    enter.setLayoutData(gridData);
	    enter.addSelectionListener(new SelectionAdapter() {
	    	public void widgetSelected(SelectionEvent event){
	    		User user =  new User();
	    		user.setName(userName.getText());
	    		user.setAddress(userAddress.getText());
	    		user.setTelephone(userPhone.getText());
	    		categories.getColumnCount();
	    		HashMap<Long, Integer> orderMap = new HashMap<Long, Integer>();	
	    		for (int i = 0; i < items.length; i++) {
	    			if (items[i].getChecked()) { 
	    				orderMap.put(Long.parseLong(items[i].getText(0).trim()), Integer.parseInt(items[i].getText(5).trim()));
	    			}
	    		} 
	    		OrderCreatingController orderCreate = new OrderCreatingController(user, orderMap);
	    		orderCreate.createOrder();
	    		onCreatingOrder(shell);
	    	}
	    });		
	}
	private void onCreatingOrder(Shell shell) {
		int style = SWT.APPLICATION_MODAL | SWT.OK | SWT.CANCEL;
		MessageBox messageBox = new MessageBox (shell, style);
		messageBox.setText ("Детали заказа");
		OrderCreatingController details = new OrderCreatingController();
		String msg = details.getMessageOrder();
		messageBox.setMessage (msg);
		messageBox.open();
	}
}
